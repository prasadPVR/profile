var board = [0, 0, 0, 0, 0, 0, 0, 0, 0];
document.value = 0;
document.count = 0;

function StartGame() {
  board = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  document.value = 0;
  document.count = 0;
  for (var i = 0; i < 9; i++) {
    document.getElementById("s" + i).innerText = "";
  }
  makeClickable();
}

function setMessage(msg) {
  document.getElementById('message').innerText = msg;
}

function Win(board) {
  var wins = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (var i = 0; i < 8; i++) {
    if (board[wins[i][0]] != 0 && board[wins[i][0]] == board[wins[i][1]] && board[wins[i][1]] == board[wins[i][2]])
      return board[wins[i][0]];
  }
  return 0;
}

function makeClickable() {
  for (var i = 0; i < 9; i++) {
    id = 's' + i;
    document.getElementById(id).setAttribute('onclick', 'nextMove(' + i + ')');
  }
}

function makeUnclickable() {
  for (var i = 0; i < 9; i++) {
    id = 's' + i;
    document.getElementById(id).onclick = function () {
      return false
    };
  }
}

function checkForWinner(board) {
  var winner = Win(board);
  if (winner == -1) {
    window.alert("You Won!!!");
    makeUnclickable();
    return 0;
  } else if (winner == 1) {
    window.alert("Computer Won!!!");
    makeUnclickable();
    return 1;
  }
  if (document.count == 8) {
    window.alert("Tie");
    makeUnclickable();
  }
  return 1;
}

function minmax(board, player) {
  var winner = Win(board);
  if (winner != 0)
    return winner * player;

  var move = -1;
  var score = -2;
  var i;
  for (i = 0; i < 9; ++i) {
    if (board[i] == 0) {
      board[i] = player;
      var Score = -minmax(board, player * (-1));
      if (Score > score) {
        score = Score;
        move = i;
      }
      board[i] = 0;
    }
  }
  if (move == -1)
    return 0;
  return score;
}

function ComputerMove(board) {
  if (checkForWinner(board)) {
    var move = -1;
    var score = -2;
    var i;
    for (i = 0; i < 9; ++i) {
      if (board[i] == 0) {
        board[i] = 1;
        var tempScore = -minmax(board, -1);
        board[i] = 0;
        if (tempScore > score) {
          score = tempScore;
          move = i;
        }
      }
    }
    board[move] = 1;
    var id = "s" + move;
    document.getElementById(id).innerText = 'O';
    checkForWinner(board)
    document.count++;
  }

}

function nextMove(value) {
  var id = 's' + value;

  //document.value = value;
  if (board[value] == 0 && document.getElementById(id).innerText == "") {
    document.getElementById(id).innerText = "X";
    board[value] = -1;
    checkForWinner(board);
    document.count++;
    //window.alert(board);
    if(document.count <=8)
    ComputerMove(board);
  }

}